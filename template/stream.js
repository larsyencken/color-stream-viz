// needs n and m
//var n = 126, // number of layers
    //m = 7, // number of samples per layer

var data0 = d3.layout.stack().offset("wiggle")(
          _(ordered_colors).map(function(c){ return color_points[c]; })
        );

var w = 1400,
    h = 900,
    mx = m - 1,
    my = d3.max(data0.concat(data0), function(d) {
      return d3.max(d, function(d) {
        return d.y0 + d.y;
      });
    });

var area = d3.svg.area()
    .x(function(d) { return d.x * w / mx; })
    .y0(function(d) { return h - d.y0 * h / my; })
    .y1(function(d) { return h - (d.y + d.y0) * h / my; });

var vis = d3.select("#chart")
  .append("svg")
    .attr("width", w)
    .attr("height", h);

vis.selectAll("path")
    .data(data0)
  .enter().append("path")
    .style("fill", function(d, i) { return ordered_colors[i]; })
    .attr("d", area);
