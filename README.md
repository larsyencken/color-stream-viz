# Stream visualisation

The visualisation is built by supplying three files in space-separated CSV
format:

   * `input/colors` must contain a comma-separated list of colors for each
     design in the set
   * `input/dates` must contain the timestamp for each design
   * `input/colorset` must contain the list of colors (with a name for each
     color)

Once these are supplied, the visualisation can be built by running `make`. If
successful, it can be viewed by opening `template/index.html`.
