import sys
from itertools import groupby
import csv

def extract_key(row):
    date, color, _count = row
    return (date, color)

rows = csv.reader(sys.stdin, delimiter=' ')

for (date, color), group in groupby(rows, extract_key):
    counts = (int(row[2]) for row in group)
    count = sum(counts)
    print date, color, count
