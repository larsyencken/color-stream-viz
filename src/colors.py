# -*- coding: utf-8 -*-
#
#  colors.py
#  analysis
#
#  Created by Lars Yencken on 2012-03-28.
#  Copyright 2012 Lars Yencken. All rights reserved.
#

"""
Objects for working interactively with colors.
"""

import os
import colorsys
import cStringIO as StringIO

import Image as Im
import requests
import colorific
from colormath.color_objects import RGBColor
import numpy as np
from scipy.spatial import KDTree

class Color(object):
    "A simple hexadeicmal color."
    def __init__(self, hexcolor):
        self.hexcolor = hexcolor

    def __repr__(self):
        return '<Color: %s>' % self.hexcolor

    def to_rgb(self):
        h = self.hexcolor
        return (int(h[1:3], 16), int(h[3:5], 16), int(h[5:7], 16))

    def is_dark(self):
        r, g, b = [x / 255.0 for x in self.to_rgb()]
        v = colorsys.rgb_to_hsv(r, g, b)[2]
        return v < 0.5

    def _repr_html_(self, background=False):
        margin = '2px 2px 2px 16px' if background else '2px'
        if self.is_dark():
            colorpatch = ' color: white;'
        else:
            colorpatch = ''

        return '<div style="width: 80px; height: 80px; border: 1px dashed black; margin: %s; display: inline-block; text-align: center; background: %s;"><div style="position: relative; top: 40%%;%s">%s</div></div>' % (
                margin,
                self.hexcolor,
                colorpatch,
                self.hexcolor,
            )

    def __cmp__(self, rhs):
        return cmp(self.hexcolor, rhs.hexcolor)

    def __eq__(self, rhs):
        return self.hexcolor == rhs.hexcolor

    def __hash__(self):
        return hash(self.hexcolor)

class ImageFile(object):
    def __init__(self, filename):
        self.filename = filename

    def _repr_html_(self):
        path = os.path.abspath(self.filename)
        return '<div><img src="%s"></div>' % path

class Swatch(object):
    "A series of colors."
    def __init__(self, colors, bgcolor=None):
        self.colors = colors
        self.bgcolor = bgcolor

    def __len__(self):
        return len(self.colors)

    def __iter__(self):
        return iter(self.colors)

    def __repr__(self):
        if self.bgcolor:
            bgpatch = ' (%s)' % self.bgcolor.hexcolor
        else:
            bgpatch = ''
        return '<Swatch: %s%s>' % (
                ', '.join(c.hexcolor for c in self.colors),
                bgpatch,
            )

    def _repr_html_(self):
        html = ''.join(c._repr_html_() for c in self.colors)
        if self.bgcolor:
            html += self.bgcolor._repr_html_(background=True)
        return html

    @classmethod
    def from_file(cls, filename):
        im = Im.open(filename)
        palette = colorific.extract_colors(im)
        return cls.from_raw_palette(palette)

    @classmethod
    def from_raw_palette(cls, raw_palette):
        bgcolor = None
        rth = colorific.rgb_to_hex
        if raw_palette.bgcolor:
            bgcolor = Color(rth(raw_palette.bgcolor.value))
        colors = [Color(rth(c.value)) for c in raw_palette.colors]
        return cls(colors, bgcolor=bgcolor)

class Design(object):
    def __init__(self, designid):
        self.designid = designid

    def _repr_html_(self):
        return '<div><img src="http://99designs.com/designs/%d-smallcrop"></div>' % self.designid

class DesignPalette(object):
    def __init__(self, design, swatch):
        self.design = design
        self.swatch = swatch

    def _repr_html_(self):
        return self.design._repr_html_() + self.swatch._repr_html_()

    def refresh(self):
        largecrop = requests.get('http://99designs.com/designs/%d-largecrop' \
                % self.design.designid).content
        im = Im.open(StringIO.StringIO(largecrop))
        palette = colorific.extract_colors(im)
        self.swatch = Swatch.from_raw_palette(palette)
        return self

#class CachingConverter(object):
    #"Convert hex colors to RGB."
    #def __init__(self):
        #self._cache = {}

    #def __getitem__(self, hexcolor):
        #rgb = self._cache.get(hexcolor)
        #if rgb is None:
            #rgb = RGBColor(*colorific.hex_to_rgb(hexcolor))
            #self._cache[hexcolor] = rgb

        #return rgb

def hex_to_rgb(hexcolor):
    rgb = RGBColor()
    rgb.set_from_rgb_hex(hexcolor)
    return rgb

class ReferencePalette(object):
    "A nearest neighbour interface to a reference set of colors."
    def __init__(self, color_to_name):
        self._names = color_to_name
        self._nearest = {} # cache of hex -> reference hex
        self._rgb = {} # cache of hex -> rgb
        for hexcolor in color_to_name.iterkeys():
            hexcolor = hexcolor.lower()
            self._nearest[hexcolor] = hexcolor
            self._rgb[hexcolor] = hex_to_rgb(hexcolor)

    def __getitem__(self, c):
        c = c.lower()

        if c in self._nearest:
            # exact or cached match, woohoo!
            return self._nearest[c]

        # slow pairwise comparison with color set
        rgb = hex_to_rgb(c)
        nearest = min(
                (rgb.delta_e(rgb2, method='cmc'), c2)
                for (c2, rgb2) in self._rgb.iteritems()
            )[1]
        self._nearest[c] = nearest

        return nearest

    @classmethod
    def from_file(cls, filename):
        color_to_name = {}
        with open(filename) as istream:
            for line in istream:
                c, name = line.rstrip('\n').split(' ', 1)
                color_to_name[c] = name
        return cls(color_to_name)

def hex_to_lab(hexcolor):
    rgb = hex_to_rgb(hexcolor)
    lab = rgb.convert_to('lab')
    return lab.lab_l, lab.lab_a, lab.lab_b

class KDReferencePalette(object):
    def __init__(self, ref_hexcolors):
        # ref_hexcolors = ["#ffab03", ...]
        self.lab_to_hex = {}
        ref_labcolors = np.zeros((len(ref_hexcolors), 3), dtype=np.float64)
        for i, hexcolor in enumerate(ref_hexcolors):
            lab = hex_to_lab(hexcolor)
            ref_labcolors[i, :] = lab
            self.lab_to_hex[lab] = hexcolor

        self.ref_labcolors = ref_labcolors
        self.kdtree = KDTree(ref_labcolors)

    def __getitem__(self, hexcolor):
        "Return the single nearest reference color."
        lab = hex_to_lab(hexcolor)
        _distances, indexes = self.kdtree.query([lab])
        i, = indexes
        ref_lab = self.ref_labcolors[i, :]
        return self.lab_to_hex[tuple(ref_lab)]

    @staticmethod
    def from_file(filename):
        # ignoring the names
        hexcolors = set()
        with open(filename) as istream:
            for line in istream:
                hexcolor, name = line.rstrip('\n').split(' ', 1)
                hexcolors.add(hexcolor)

        return KDReferencePalette(sorted(hexcolors))

