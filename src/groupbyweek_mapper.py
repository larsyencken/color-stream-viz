import sys
import csv

rows = csv.reader(sys.stdin, delimiter="\t")

# line => tab sep: ts, designid, [comma sep: colors]
for ts, _designid, colors, bgcolor in rows:
    colors = colors.split(",")

    month = int(ts) / (3600*24*7)
    for c in colors:
        print month, c, 1
