import sys
import csv
import datetime

rows = csv.reader(sys.stdin, delimiter="\t")

# line => tab sep: ts, designid, [comma sep: colors] 
for ts, _designid, colors, bgcolor in rows:
    colors = colors.split(",")

    month = datetime.datetime.utcfromtimestamp(int(ts)).strftime('%Y-%m')
    for c in colors:
        print month, c, 1
