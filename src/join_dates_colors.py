# -*- coding: utf-8 -*-
#
#  join_dates_colors.py
#  pre2012-logo-full
#

import os
import sys

def join_dates_colors(colors_file, date_file, dest_dir):
    ok_out = os.path.join(dest_dir, 'colors.dated')
    err_out = os.path.join(dest_dir, 'colors.missing_dates')

    timestamps = {}
    print 'Loading timestamps...'
    with open(date_file) as istream:
        pairs = (l.split() for l in istream)
        for designid, timestamp in pairs:
            designid = int(designid)
            timestamp = int(timestamp)
            timestamps[designid] = timestamp

    print 'Timestamping design swatches...'
    missing = open(err_out, 'w')
    with open(colors_file) as istream:
        tuples = (l.rstrip('\n').split('\t') for l in istream)
        with open(ok_out, 'w') as ostream:
            for designid, colors, bgcolor in tuples:
                try:
                    print >> ostream, '\t'.join((
                        str(timestamps[int(designid)]),
                        designid,
                        colors,
                        bgcolor
                    ))
                except KeyError:
                    print >> missing, '\t'.join((
                            designid,
                            colors,
                            bgcolor,
                        ))

    print 'Done'

if __name__ == '__main__':
    join_dates_colors(*sys.argv[1:])
