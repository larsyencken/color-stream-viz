import sys
import csv
from collections import Counter
import itertools

dist = Counter()

for t, designid, colors, bgcolor in csv.reader(sys.stdin, delimiter='\t'):
    colors = colors.split(',')
    if len(colors) > 1:
        for a, b in itertools.combinations(colors, 2):
            if a < b:
                p = (a, b)
            else:
                p = (b, a)

            dist[p] += 1

for (c1, c2), v in dist.most_common(50):
    print c1, c2, v

